package in.forsk;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

public class ImageLoaderAsyncTask extends AsyncTask<String, Integer, Bitmap> {

    Context context;
    ImageView imageView;
    String url = "";

    //22222222222222222222
    ProgressDialog pd;


    public ImageLoaderAsyncTask(Context context, String url, ImageView imageView) {
        this.context = context;
        this.url = url;
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        //22222222222222222222222222
        pd = new ProgressDialog(context);
        pd.setMessage("Loading Image  0%..");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        InputStream in = Utils.openHttpConnection(url);
        Bitmap bmp = BitmapFactory.decodeStream(in);


        for (int i = 1; 10 > i; i++) {
            try {
                onProgressUpdate(i);
                Thread.sleep(500);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return bmp;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        //33333333333333333333333333333333333
        //Looper,Message
        final int progress = values[0] * 10;

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd.setMessage("Loading Image  " + progress + "%..");
            }
        });

    }

    @Override
    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);

        if (result != null)
            imageView.setImageBitmap(result);

        //22222222222222222222222222222222
        if (pd != null)
            pd.dismiss();

    }


}
